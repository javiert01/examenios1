//
//  ArregloUsuarios.swift
//  FrontEnd
//
// 

import Foundation
import AlamofireImage
import Alamofire

class Pelicula {
    let original_title:String
    let overview:String
    let poster_path:String
    let vote_count:Int
    
    init(original_title:String,overview:String,poster_path:String,vote_count:Int){
        self.original_title = original_title
        self.overview = overview
        self.vote_count = vote_count
        self.poster_path = poster_path
        
      
        
    }
}
