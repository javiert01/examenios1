//
//  UsuariosTableViewCell.swift
//  FrontEnd
//


import UIKit

class UsuariosTableViewCell: UITableViewCell {
   
    @IBOutlet weak var popularidadLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var imagePeli: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
