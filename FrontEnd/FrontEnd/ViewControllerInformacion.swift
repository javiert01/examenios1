//
//  ViewControllerInformacion.swift
//  FrontEnd
//
//

import UIKit
import AlamofireImage
import Alamofire

class ViewControllerInformacion: UIViewController {
    
    let backend = BackEndManager()
    
    @IBOutlet weak var textInfo: UITextView!
    @IBOutlet weak var labelNombreInfo: UILabel!
    @IBOutlet weak var imgInfo: UIImageView!
    @IBOutlet weak var labelPopularidad: UILabel!
    @IBOutlet weak var guardarButton: UIButton!
    
    var nombreInfo = ""
    var imagenPelicula = UIImage()
    var resumen = ""
    var urlImg = ""
    var popularidadTxt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
         labelNombreInfo.text = nombreInfo
        textInfo.text = resumen
        labelPopularidad.text = "\(popularidadTxt)"
        let url = "http://image.tmdb.org/t/p/w185/\(urlImg)"
        Alamofire.request(url).responseImage { response in
            guard let image = response.result.value else {
                return
            }
            self.imgInfo.image = image
        }

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func guardarPeliculaBackEnd(_ sender: Any) {
        let urlString = "http://localhost:1337/Pelicula"
        
        let parameters: Parameters = [
            "nombre": labelNombreInfo.text ?? "",
            "overview": textInfo.text ?? "",
            "urlImagen": urlImg,
            "popularidad": popularidadTxt
            
        ]
        
        
        Alamofire.request(urlString,method:.post,parameters:parameters).responseJSON { response in
            
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                
                
            }
            
        }
       

    }
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
