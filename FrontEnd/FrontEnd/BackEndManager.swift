//
//  BackEndManager.swift
//  FrontEnd
//
//
//

import UIKit
import Alamofire

class BackEndManager: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func  solicitarBackEnd(urlString:String){
            Alamofire.request(urlString).responseJSON { response in
                
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
        
            }

            }
  
    }
    

    
    func consultarDatos(){
        
        Alamofire.request("http://api.themoviedb.org/3/discover/movie?%20%E2%86%B5%20sort_by=popularity.desc?&api_key=a57c1df94fab8edbe478896b56fa40a1").responseJSON { response in
            
            if let JSON = response.result.value {
                print(type(of: JSON))
                let jsonDict = JSON as! NSDictionary;
                let jsonArr = jsonDict["results"] as! NSArray
                 print(jsonArr)
                var array = [Pelicula]()
                
                
                for i in jsonArr {
                    let dict = i as! NSDictionary
                    
                    array.append(Pelicula(original_title:dict["original_title"]! as! String,overview:dict["overview"]! as! String,poster_path:dict["poster_path"]! as! String, vote_count:dict["vote_count"]! as! Int))
                
                    
                }
                NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil, userInfo: ["listo":array])
                
                print("se envio!!")
                
            }
            
        }
        
    }
    
    
    func consultarDatosBackEnd(){
        
        Alamofire.request("http://localhost:1337/Pelicula").responseJSON { response in
            
            if let JSON = response.result.value {
                print(type(of: JSON))
                let jsonDict = JSON as! NSArray;
                var array = [Pelicula]()
                
                
                for i in jsonDict {
                    let dict = i as! NSDictionary
                    
                    array.append(Pelicula(original_title:dict["nombre"]! as! String,overview:dict["overview"]! as! String,poster_path:dict["urlImagen"]! as! String,vote_count:dict["popularidad"]! as! Int))
                    
                    
                }
                
                
                NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil, userInfo: ["listo":array])
                
                print("se envio!!")
                
            }
            
        }
        
    }
    
}
