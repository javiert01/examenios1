//
//  PeliculasGuardadasTableViewCell.swift
//  FrontEnd
//
//  Created by Javier Tinoco on 12/6/17.
// 
//

import UIKit

class PeliculasGuardadasTableViewCell: UITableViewCell {

    @IBOutlet weak var imagenG: UIImageView!
    @IBOutlet weak var popularidadG: UILabel!
    @IBOutlet weak var nombreG: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
