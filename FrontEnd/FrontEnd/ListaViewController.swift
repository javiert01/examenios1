//
//  ListaViewController.swift
//  FrontEnd
//


import UIKit
import AlamofireImage
import Alamofire

class ListaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    var usuarios  = [Pelicula]()
    
    @IBOutlet weak var tablaUsuarios: UITableView!
    

    override func viewWillAppear(_ animated: Bool) {
//        nombresUsuarios.append("Nombre \(n)")
//        apellidosUsuarios.append("Apellido \(n)")
//        cedulasUsuarios.append("Cedula \(n)")
//        n = n + 1
        //actualizacion desde el backend
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizar"), object: nil)
        

    }
    
    func actualizarInformacion(_ notification: Notification){
        print("actualizandoo ....")
        usuarios = notification.userInfo?["listo"] as! [Pelicula]
        tablaUsuarios.reloadData()
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"),object:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let backend = BackEndManager()
        print("consultando datos")
        backend.consultarDatos()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaUsuario") as! UsuariosTableViewCell
        
        cell.nombreLabel.text = usuarios[indexPath.row].original_title
        cell.popularidadLabel.text =  "\(usuarios[indexPath.row].vote_count)"
        let url = "http://image.tmdb.org/t/p/w185/\(usuarios[indexPath.row].poster_path)"
        Alamofire.request(url).responseImage { response in
            guard let image = response.result.value else {
                return
            }
            cell.imagePeli.image = image
            
            
        }
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="informacionSegue"{
            let destinationController = segue.destination as!ViewControllerInformacion
            let path = tablaUsuarios.indexPathForSelectedRow
            let valor = usuarios[(path?.row)!].original_title
            print(valor)
            
            destinationController.nombreInfo = valor
            destinationController.resumen = usuarios[(path?.row)!].overview
            destinationController.popularidadTxt = usuarios[(path?.row)!].vote_count
            destinationController.urlImg = usuarios[(path?.row)!].poster_path
            }
        
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
