//
//  ListaPeliculaGuardadasTableViewController.swift
//  FrontEnd
//
//  Created by Javier Tinoco on 12/6/17.
//

import UIKit
import AlamofireImage
import Alamofire

class ListaPeliculaGuardadasTableViewController: UITableViewController {

     var usuarios  = [Pelicula]()
    @IBOutlet var tablaUsuarios: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        //        nombresUsuarios.append("Nombre \(n)")
        //        apellidosUsuarios.append("Apellido \(n)")
        //        cedulasUsuarios.append("Cedula \(n)")
        //        n = n + 1
        //actualizacion desde el backend
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizar"), object: nil)
        
        
    }
    
    func actualizarInformacion(_ notification: Notification){
        print("actualizandoo ....")
        usuarios = notification.userInfo?["listo"] as! [Pelicula]
        tablaUsuarios.reloadData()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"),object:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let backend = BackEndManager()
        print("consultando datos")
        backend.consultarDatosBackEnd()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return usuarios.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "peliculasGuardadasIdentifier") as! PeliculasGuardadasTableViewCell

        cell.nombreG.text = usuarios[indexPath.row].original_title
        cell.popularidadG.text = "\(usuarios[indexPath.row].vote_count)"
        let url = "http://image.tmdb.org/t/p/w185/\(usuarios[indexPath.row].poster_path)"
        Alamofire.request(url).responseImage { response in
            guard let image = response.result.value else {
                return
            }
            cell.imagenG.image = image
            
            
        }
        
        
        
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="infoMisPeliculasSegue"{
            let destinationController = segue.destination as!InfoGuardarViewController
            let path = tablaUsuarios.indexPathForSelectedRow
            let valor = usuarios[(path?.row)!].original_title
            print(valor)
            
            destinationController.nombrePeli = valor
            destinationController.resumenPeli = usuarios[(path?.row)!].overview
            destinationController.popularidad = usuarios[(path?.row)!].vote_count
            destinationController.urlImagen = usuarios[(path?.row)!].poster_path
        }
        
        
    }
    

 

}
