//
//  InfoGuardarViewController.swift
//  FrontEnd
//
//  Created by Javier Tinoco on 15/6/17.
//  
//

import UIKit
import Alamofire
import AlamofireImage

class InfoGuardarViewController: UIViewController {

    @IBOutlet weak var nombrePeliLabel: UILabel!
    @IBOutlet weak var infoTxtView: UITextView!
    @IBOutlet weak var peliculaImageView: UIImageView!
    @IBOutlet weak var popularidadLabel: UILabel!
    
    var popularidad = 0
    var imagenPeli = UIImage()
    var resumenPeli = ""
    var urlImagen = ""
    var nombrePeli = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nombrePeliLabel.text = nombrePeli
        infoTxtView.text = resumenPeli
        popularidadLabel.text = "\(popularidad)"
        let url = "http://image.tmdb.org/t/p/w185/\(urlImagen)"
        Alamofire.request(url).responseImage { response in
            guard let image = response.result.value else {
                return
            }
            self.peliculaImageView.image = image
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
